
import java.lang.*;
import java.io.*;
import java.util.*;
public class Test{//主程序
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("请输入学生人数：");
        Student[] stuArr = new Student[in.nextInt()];
        Operation adminStu = new Operation();
        while(true){
            System.out.println("-----请选择你要执行的功能-----");
            System.out.println("1：添加一个学生");
            System.out.println("2：查找一个学生");
            System.out.println("3：根据学生编号更新学生基本信息");
            System.out.println("4：录入学生成绩");
            System.out.println("5：根据成绩进行排序");
            System.out.println("6:删除学生信息");
            System.out.println("7：退出系统");

            String number = in.next();
            if(number.equals("1")){
                System.out.println("请输入学生的学号：");
                int num = in.nextInt();
                System.out.println("请输入学生的姓名：");
                String name = in.next();
                System.out.println("请输入学生的成绩：");
                int grade = in.nextInt();
                try{
                    adminStu.Create(num,name,grade,stuArr);//添加学生
                }
                catch(wrongException e){
                    System.out.println("出现错误");
                    System.out.println(e.warnMess());
                }
            }else if(number.equals("2")){
                System.out.println("请输入学生的学号进行查找：");
                int num = in.nextInt();
                try{
                    adminStu.find(num,stuArr);//查找学生
                }
                catch(wrongException e){
                    System.out.println("出现错误");
                    System.out.println(e.warnMess());
                }
            }else if(number.equals("3")){
                System.out.println("请输入需要更改的学生的学号：");
                int num = in.nextInt();
                System.out.println("请输入学生的姓名：");
                String name = in.next();
                System.out.println("请输入学生的成绩：");
                int grade = in.nextInt();
                try{
                    adminStu.update(num,name,grade,stuArr);//更新学生基本信息
                }
                 catch(wrongException e){
                    System.out.println("出现错误");
                    System.out.println(e.warnMess());
                }
            }else if(number.equals("4")){
                System.out.println("执行输入成绩操作");
                System.out.println("请输入学生的学号：");
                int num = in.nextInt();
                try {
                    adminStu.input(num, stuArr);//输入成绩
                }
                catch(wrongException e){
                    System.out.println("出现错误");
                    System.out.println(e.warnMess());
                }
            }else if(number.equals("5")){
                System.out.println("根据成绩排序操作");
                adminStu.Sort(stuArr);//按总分排序
            }else if(number.equals("6")) {
                System.out.println("请输入需要删除的学生学号");
                int num = in.nextInt();
                try {
                    adminStu.delete(num, stuArr);
                }
                catch(wrongException e){
                    System.out.println("出现错误");
                    System.out.println(e.warnMess());
                }
            } else if(number.equals("7")){
                System.out.println("程序已退出");
                //break;
                System.exit(0);
            }
        }
    }
}
