import java.util.*;
public class Operation {//用来管理学生的一个类

    public boolean exist(int num, Student stu) {//判断学生是否存在的方法
        if (stu != null) {
            if (stu.getID() == num) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    public void Create(int num,String name,int grade,Student[] arr)throws wrongException{//添加学生的方法
        Student stu = new Student();
        stu.setID(num);
        stu.setName(name);
        stu.setGrade(grade);
        int i = this.setIndex(arr);
        if(i==99999){
            throw new wrongException("学生人数已添满，不能再添加了");
        }
          else {
            arr[i] = stu;
        }

        System.out.println("学号：" + arr[i].getID() + " 姓名：" + arr[i].getName() + " 成绩: " + arr[i].getGrade());
    }
    public int setIndex(Student[] arr){//返回数组为空的下标
        for(int i=0;i<arr.length;i++){
            if(arr[i]==null){
                return i;
            }
        }
        return 99999;
    }



    public void delete(int num, Student[] arr) throws wrongException {//删除学生成绩

        for (int i = 0; i < arr.length; i++) {
            if (this.exist(num, arr[i])) {
                if (arr[i].getID() == num) {
                    arr[i] = null;
                    System.out.println("已成功删除");
                    return;
                }
            }
        }
        throw new wrongException("没有这个学生存在");
    }

    public void find(int num, Student[] arr) throws wrongException {//查询学生的方法
        for (int i = 0; i < arr.length; i++) {//判断学生是否存在
            if (this.exist(num, arr[i])) {
                System.out.println("有这个学生，信息如下");
                System.out.println("学号：" + arr[i].getID() + " 姓名：" + arr[i].getName() + " 成绩: " + arr[i].getGrade());
                return;
            }
        }
        throw new wrongException("没有这个学生的存在");
    }

    public void update(int num, String name, int grade, Student[] arr) throws wrongException {//更新学生基本信息的方法
        for (int i = 0; i < arr.length; i++) {
            if (this.exist(num, arr[i])) {
                arr[i].setName(name);
                arr[i].setGrade(grade);
                System.out.println("更新学生信息成功！");
                return;
            }
        }
        throw new wrongException("没找到这个学生更新信息失败");
    }

    public void input(int num, Student[] arr) throws wrongException {//输入学生成绩的方法
        for (int i = 0; i < arr.length; i++) {
            if (this.exist(num, arr[i])) {

                System.out.println("请输入成绩：");
                Scanner in = new Scanner(System.in);
                if (in.hasNextInt()) {//输入非整形数则不执行
                    arr[i].setGrade(in.nextInt());
                } else {
                    return;
                }
                System.out.println("学号：" + arr[i].getID() + " 姓名：" + arr[i].getName() + " 成绩: " + arr[i].getGrade());
                return;
            }
        }
    }

    public void Sort(Student[] arr) {//根据成绩从高到低排序的方法
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] != null && arr[j] != null) {
                    if (arr[i].getGrade() < arr[j].getGrade()) {
                        Student t = arr[i];
                        arr[i] = arr[j];
                        arr[j] = t;
                    }

                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null) {
                System.out.println("学号：" + arr[i].getID() + " 姓名：" + arr[i].getName() + " 成绩: " + arr[i].getGrade());
            }
        }
    }
}
