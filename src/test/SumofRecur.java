import java.util.Scanner;
public class SumofRecur {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n <= 0)
            System.out.println("错误！请重新输入");
        else {
            int sum = 0, i;
            for (i = 1; i <= n; i++) {

                sum += fact(i);
            }
            System.out.println("sum=" + sum);


        }
    }
    public static int fact(int i) {
        if (i == 0)
            return 1;
        else
            return i * fact(i - 1);

    }
}
