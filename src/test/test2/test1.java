
public class test1 {
    public static void main(String args[]) {
        int n=Integer.parseInt(args[0]);
        int m=Integer.parseInt(args[1]);
        int result = C(n,m);
        System.out.println(result);
    }

    public static int C(int n,int m) {

        if(n<m||n<0||m<0){
            System.out.println("输入错误!");
            return 0;
        }
        else if(m==1){
            return n;
        }
        else if(n==m||m==0) {
            return 1;
        }

        else {
            return C(n-1,m-1)+C(n-1,m);
        }
    }

}
