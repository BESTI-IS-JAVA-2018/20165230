/*      1. 除了Test类，PC，CPU，HardDisk要实现多个构造方法，比如PC类要有PC(), PC(CPU cpu), PC(HardDisk HD), PC(CPU cpu, HardDisk HD)
        2. 除了Test类，PC，CPU，HardDisk要覆盖toString(), equals()方法
        3. Test类中要测试到所有类的toString(), equals()方法
*/
public class CPU {
    int speed;
    int getSpeed(){
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
    CPU(){

    }
    @Override
    public String toString() {
        return "测试CPU的toString()方法："+super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}

