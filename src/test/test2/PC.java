/*      1. 除了Test类，PC，CPU，HardDisk要实现多个构造方法，比如PC类要有PC(), PC(CPU cpu), PC(HardDisk HD), PC(CPU cpu, HardDisk HD)
        2. 除了Test类，PC，CPU，HardDisk要覆盖toString(), equals()方法
        3. Test类中要测试到所有类的toString(), equals()方法
*/
public class PC {
    CPU cpu;
    HardDisk HD;
    public void setCpu(CPU cpu) {
        this.cpu = cpu;
    }

    public void setHardDisk(HardDisk HD) {
        this.HD = HD;
    }
    void show(){
        System.out.println("CPU的速度"+cpu.getSpeed());
        System.out.println("CPU的容量"+HD.getAmount());
    }
    PC(){

    }
     PC(CPU cpu){

    }
     PC(HardDisk HD){

    }
    PC(CPU cpu, HardDisk HD){

    }

    @Override
    public String toString() {
        return  "测试PC的toString()方法："+super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}

