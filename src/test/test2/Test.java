/*      1. 除了Test类，PC，CPU，HardDisk要实现多个构造方法，比如PC类要有PC(), PC(CPU cpu), PC(HardDisk HD), PC(CPU cpu, HardDisk HD)
        2. 除了Test类，PC，CPU，HardDisk要覆盖toString(), equals()方法
        3. Test类中要测试到所有类的toString(), equals()方法
*/
public class Test {
    public static void main(String[] args) {
        CPU cpu = new CPU();
        cpu.setSpeed(2200);
        HardDisk disk = new HardDisk();
        disk.setAmount(200);
        PC pc = new PC();
        pc.setCpu(cpu);
        pc.setHardDisk(disk);
        pc.show();
        /* 测试toString()方法*/
        System.out.println(cpu.toString());
        System.out.println(disk.toString());
        System.out.println(pc.toString());
        /* 测试equals()方法*/
        CPU cpu1 = new CPU();
        cpu1.setSpeed(2200);
        System.out.println(cpu1.equals(cpu));
        HardDisk disk1 = new HardDisk();
        disk1.setAmount(220);
        System.out.println(disk1.equals(disk));
        PC pc1 = new PC();
        pc1.setCpu(cpu1);
        pc1.setHardDisk(disk1);
        System.out.println(pc1.equals(pc));
        pc1.show();
    }
}

