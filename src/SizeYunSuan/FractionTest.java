import junit.framework.TestCase;
import static org.junit.Assert.*;
public class FractionTest extends TestCase {
    public void testAdd() {
        assertEquals(1/3, 2/3+(-1/3));
        assertEquals(0, 1 / 3 + (-1 / 3));
        assertEquals(-2 / 3, -1 / 3 + (-1 / 3));
    }

    public void testSubtract() {
        assertEquals(5 / 9, 8 / 9 - 1 / 3);
        assertEquals(0, 8 / 9 - 8 / 9);
        assertEquals(-5 / 9, -7 / 9 - (-2 / 9));
    }

    public void testMultiply() {
        assertEquals(4 / 9, 2 / 3 * 2 / 3);
        assertEquals(0, 0 * 2 / 3);
        assertEquals(-2 / 3, 1 / 3 * (-2));
    }

    public void testDivide() {
        assertEquals(6 / 17, 12 / 17 / 1 / 2);
        assertEquals(0, 0 / 12 / 17);
        assertEquals(-6 / 17, -6 / 17);
    }

}

