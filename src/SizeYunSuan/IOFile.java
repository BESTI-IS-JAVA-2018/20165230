import java.io.*;
public class IOFile {
    PrintStream  ps;
    public IOFile(String file){
        try {
            ps = new PrintStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void WriteQuestionsToFile(String s){
        ps.append(s);
    }
}
