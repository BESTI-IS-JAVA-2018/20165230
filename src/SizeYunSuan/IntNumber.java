mport java.util.Random;
public class IntNumber {
    private int A;
    public IntNumber(int A){
        this.A = A;
    }

    public int add(IntNumber op1){
        int result = A + op1.A;
        System.out.print(A + " + " + op1.A + "=");
        return result;
    }

    public int subtract(IntNumber op1){
        int result = A - op1.A;
        System.out.print(A + " - " + op1.A + "=");
        return result;
    }

    public int multiply(IntNumber op1){
        int result = A * op1.A;
        System.out.print(A + " * " + op1.A + "=");
        return result;
    }


    public String divide(Fraction op1){
        System.out.print(op1.getNumerator() + " / " + op1.getDenominator() + "=");
        return op1.toString();
    }

    public static IntNumber obj(){
        Random ran = new Random();
        return new IntNumber(ran.nextInt(100));
    }
}
