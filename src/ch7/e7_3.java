interface SpeakHello{
    void speak();
}
class HelloMachine{
    protected void turnOn(SpeakHello hello){
        hello.speak();
    }
}
public class e7_3 {
    public static void main(String[] args) {
        HelloMachine machine = new HelloMachine();
        machine.turnOn(new SpeakHello() {
            @Override
            public void speak() {
                System.out.println("hello,you are welcome!");
            }
        });
        machine.turnOn(new SpeakHello() {
            @Override
            public void speak() {
                System.out.printf("你好，欢迎光临");
            }
        });
    }



}

