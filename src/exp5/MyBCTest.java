import java.util.Scanner;
import static org.junit.Assert.*;
import java.util.Scanner;

public class MyBCTest  {
    public static void main (String[] args) {
        String str1=null,str2=null, again=null;
        int result,i=0,length=0;

        try
        {
            Scanner in = new Scanner(System.in);

            do
            {
                MyDC evaluator = new MyDC();
                MyBC turner = new MyBC();
                System.out.println ("请输入有效的中缀表达式: ");
                str1 = in.nextLine();
                str2 = turner.turn(str1);
                while(str2.charAt(i)!='\0'){
                    length++;
                    i++;
                }
                //length 表示字符串包括‘\0’的字符个数
                str2 = str2.substring(1,length-1);
                //System.out.println(expression2);
                result = evaluator.evaluate (str2);
                System.out.println();
                System.out.println ("得到的结果是 " + result);

                System.out.print ("是否继续进行计算 [Y/N]? ");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}