import java.util.Objects;
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Long extends Data {
    long value;
    Long() {
        value=(long)20165230;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Byte extends Data {
    byte value;
    Byte() {
        value=(byte)20165230;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class LongFactory extends Factory {
    public Data CreateDataObject(){
        return new Long();
    }
}
class ByteFactory extends Factory {
    public Data CreateDataObject(){
        return new Byte();
    }
}
//Client classes
class Document {
    Data data;
    Document(Factory factory){
        data = factory.CreateDataObject();
    }
    public void DisplayData(){
        data.DisplayValue();

    }
}
public class MyDoc {
    static Document d;
    static Document c;
    public static void main(String[] args) {
        d = new Document(new ByteFactory());
        d.DisplayData();
        c = new Document(new LongFactory());
        c.DisplayData();
    }
}
