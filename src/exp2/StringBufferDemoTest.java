import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.*;
public class StringBufferDemoTest extends TestCase {
    StringBuffer string1 = new StringBuffer("tiankunye");//测试9个字符
    StringBuffer string2 = new StringBuffer("tiankunye is learning");//测试21个字符
    StringBuffer string3 = new StringBuffer("tiankunye is learning java");//测试26个字符
    @Test
    public void testCharAt(){
        assertEquals('t',string1.charAt(0));
        assertEquals('k',string2.charAt(4));
        assertEquals('y',string3.charAt(7));
    }
    @Test
    public void testCapacity(){
        assertEquals(25,string1.capacity());
        assertEquals(37,string2.capacity());
        assertEquals(42,string3.capacity());
    }
    @Test
    public void testindexOf() {
        assertEquals(4, string1.indexOf("kun"));
        assertEquals(10, string2.indexOf("is"));
        assertEquals(22, string3.indexOf("java"));
    }
    @Test
    public void testlength() {
        assertEquals(9, string1.length());
        assertEquals(21, string2.length());
        assertEquals(26, string3.length());
    }
}
